// React
import React from "react";
// react-icons
import { FaMale, FaFemale, FaChild } from "react-icons/fa";

// next
import Link from "next/link";
import { useRouter } from "next/router";

// clsx
import clsx from "clsx";

// materialui
import {
  Grid,
  Typography,
  Breadcrumbs,
  Button,
  Chip,
  Avatar,
  Divider,
  Hidden,
} from "@material-ui/core";
import { deepOrange } from "@material-ui/core/colors";
import MenuIcon from "@material-ui/icons/Menu";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import FavoriteIcon from "@material-ui/icons/Favorite";
import PersonIcon from "@material-ui/icons/Person";

import {
  emphasize,
  makeStyles,
  fade,
  useTheme,
} from "@material-ui/core/styles";

// component
import CustomLink from "../links/customLink";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > * + *": {
      marginLeft: theme.spacing(4),
    },
    padding: theme.spacing(2, 0),
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  chip: {
    textTransform: "uppercase",
    height: theme.spacing(3),
    color: theme.palette.grey[800],
    backgroundColor: theme.palette.grey[100],
    fontWeight: theme.typography.fontWeightRegular,
    "&:hover, &:focus": {
      backgroundColor: theme.palette.grey[300],
    },
    "&:active": {
      backgroundColor: emphasize(theme.palette.grey[300], 0.12),
    },
  },
  orange: {
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: deepOrange[500],
  },
  activeChip: (active) => {
    return active
      ? {
          color: theme.palette.secondary.main,
        }
      : {};
  },
}));

const productMenus = [
  {
    name: "Deals!",
    href: "/deals",
    avatar: "D",
  },
  {
    name: "New Arrivals",
    href: "/newarrivals",
    avatar: "N",
  },
  {
    name: "Best Sellers",
    href: "/bestsellers",
    avatar: "B",
  },
  {
    name: "Kids",
    href: "/kids",
    avatar: "K",
  },
  {
    name: "Men",
    href: "/men",
    avatar: "M",
  },
  {
    name: "Women",
    href: "/women",
    avatar: "W",
  },
];

function CategoryChip({ productMenu }) {
  const router = useRouter();
  const classes = useStyles(router.asPath === productMenu.href);

  return (
    <Link href={productMenu.href} passHref>
      <Chip
        className={clsx(classes.chip, classes.activeChip)}
        avatar={<Avatar>{productMenu.avatar}</Avatar>}
        label={productMenu.name}
        size="small"
        variant="default"
        component="a"
        clickable
      />
    </Link>
  );
}

export default function SecondaryNavigation({ divider = true }) {
  const classes = useStyles();

  return (
    <Hidden smDown>
      <Grid item>
        {divider && <Divider variant="fullWidth" />}

        <div className={classes.root}>
          {productMenus.map((productMenu, key) => (
            <CategoryChip key={key} productMenu={productMenu} />
          ))}
        </div>
      </Grid>
    </Hidden>
  );
}

export { productMenus };
