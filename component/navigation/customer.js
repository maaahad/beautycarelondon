// react
// next

// clsx
import clsx from "clsx";
// mui
import { Grid, IconButton, Badge, Avatar } from "@material-ui/core";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import FavoriteIcon from "@material-ui/icons/Favorite";
import PersonIcon from "@material-ui/icons/Person";
import { makeStyles, withStyles } from "@material-ui/core/styles";

// css
const useStyles = makeStyles((theme) => ({
  hideOnXS: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  iconButton: {
    "&:hover": {
      color: theme.palette.secondary.main,
    },
  },
}));

const StyledBadge = withStyles((theme) => ({
  badge: {
    right: -3,
    top: 13,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: "0 4px",
  },
}))(Badge);

export default function Customer({ loggedIn }) {
  const classes = useStyles();
  return (
    <>
      <Grid item>
        <IconButton size="medium" className={classes.iconButton}>
          <StyledBadge badgeContent="4" color="primary" variant="standard">
            <ShoppingCartIcon />
          </StyledBadge>
        </IconButton>
      </Grid>
      <Grid item>
        <IconButton
          size="medium"
          className={clsx(classes.hideOnXS, classes.iconButton)}
        >
          <StyledBadge badgeContent="0" color="primary">
            <FavoriteIcon />
          </StyledBadge>
        </IconButton>
      </Grid>
      <Grid item>
        <IconButton
          size="medium"
          className={clsx(classes.hideOnXS, classes.iconButton)}
        >
          <StyledBadge
            color="primary"
            invisible={!loggedIn}
            badgeContent={loggedIn && "M"}
          >
            <PersonIcon />
          </StyledBadge>
        </IconButton>
      </Grid>
    </>
  );
}
