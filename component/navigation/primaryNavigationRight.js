// react
import React, { useState } from "react";
// next

// mui
import { Grid, IconButton, Badge } from "@material-ui/core";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import FavoriteIcon from "@material-ui/icons/Favorite";
import PersonIcon from "@material-ui/icons/Person";
import { makeStyles } from "@material-ui/core/styles";

// component
import Customer from "./customer";

// TODO: In future : we will include a search Bar here

export default function PrimaryNavigationRight() {
  const [loggedIn, setLoggedIn] = useState(true);

  return <Customer loggedIn={loggedIn} />;
}
