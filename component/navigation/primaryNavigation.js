// React
import React, { useState, useEffect, useLayoutEffect } from "react";
// next

// materialui
import {
  Grid,
  IconButton,
  Typography,
  Paper,
  Link,
  Hidden,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import FavoriteIcon from "@material-ui/icons/Favorite";
import PersonIcon from "@material-ui/icons/Person";

import { makeStyles, fade, useTheme } from "@material-ui/core/styles";

// components
import PrimaryNavigationRight from "./primaryNavigationRight";
import PrimaryNavigationLeft from "./primaryNavigationLeft";
import Brand from "./brand";
import BCLMenu from "./menu";

const useStyles = makeStyles((theme) => ({
  menu: {
    // backgroundColor: "red",
  },
  rightNav: {
    "& > * + *": {
      // marginLeft: theme.spacing(2),
    },
  },
}));

export default function PrimaryNavigation() {
  const classes = useStyles();
  const theme = useTheme();

  // TODO: Control spacing for RightNav Grid spacing when there is only grid item.
  const [pnrGridSpacing, setPnrGridSpacing] = useState(1);

  const handlePnrGridSpacing = (event) => {
    const viewportWidth = window.innerWidth;
    viewportWidth < theme.breakpoints.values["sm"]
      ? setPnrGridSpacing(0)
      : setPnrGridSpacing(1);
  };

  useEffect(() => {
    window.addEventListener("resize", handlePnrGridSpacing);
    handlePnrGridSpacing();
    return () => window.removeEventListener("resize", handlePnrGridSpacing);
  }, []);

  return (
    <Grid
      item
      container
      direction="row"
      alignItems="center"
      justify="space-between"
      spacing={1}
    >
      <Hidden mdUp>
        <Grid
          item
          container
          xs={4}
          md={4}
          direction="row"
          alignItems="center"
          justify="flex-start"
          spacing={1}
        >
          <BCLMenu />
        </Grid>
      </Hidden>

      <Hidden smDown>
        <Grid
          item
          container
          xs={4}
          md={4}
          direction="row"
          alignItems="center"
          justify="flex-start"
          spacing={1}
        >
          <PrimaryNavigationLeft />
        </Grid>
      </Hidden>
      <Grid
        item
        container
        xs={4}
        md={4}
        direction="row"
        alignItems="center"
        justify="center"
        spacing={1}
      >
        <Brand />
      </Grid>
      <Grid
        item
        container
        direction="row"
        xs={4}
        md={4}
        className={classes.rightNav}
        alignItems="center"
        justify="flex-end"
        spacing={pnrGridSpacing}
      >
        <PrimaryNavigationRight />
      </Grid>
    </Grid>
  );
}
