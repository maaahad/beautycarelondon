// react

// nextjs

// mui
import { Grid, InputBase } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { fade, makeStyles } from "@material-ui/core/styles";

// css

const useStyles = makeStyles((theme) => ({
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.black, 0.05),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.black, 0.1),
    },
    width: 10,
    [theme.breakpoints.up("md")]: {
      width: "auto",
      marginBottom: "0",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em  + ${theme.spacing(4)}px)`,
    // width: "100%",
    transition: theme.transitions.create("width"),
    [theme.breakpoints.up("md")]: {},
  },
}));

export default function Search() {
  const classes = useStyles();
  return (
    <Grid item className={classes.search} xs={9}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        placeholder="Search here"
        color="primary"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ "aria-label": "search" }}
        fullWidth
      />
    </Grid>
  );
}
