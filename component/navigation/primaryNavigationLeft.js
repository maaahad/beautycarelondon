// react

// next
import Link from "next/link";

// mui
import { Grid, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// component
import ActiveableLink from "../links/activeableLink";

// css

export default function PrimaryNavigationLeft() {
  return (
    <>
      <Grid item>
        <ActiveableLink href="/" label="Home" />
      </Grid>
      <Grid item>
        <ActiveableLink href="/contact" label="Contact" />
      </Grid>
      <Grid item>
        <ActiveableLink href="/blogs" label="Blogs" />
      </Grid>
    </>
  );
}
