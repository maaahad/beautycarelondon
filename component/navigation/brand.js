// react

// next
import Image from "next/image";

// mui
import { Grid, Typography, Paper } from "@material-ui/core";

import { makeStyles } from "@material-ui/core/styles";

// css
const useStyles = makeStyles((theme) => ({
  hodeOnXS: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  logo: {},
}));

export default function Brand() {
  const classes = useStyles();
  return (
    <>
      <Grid item>
        <Paper elevation={1} variant="elevation" style={{ height: "24px" }}>
          <Image src="/images/logos/bcl.jpeg" width={24} height={24} />
        </Paper>
      </Grid>
      <Grid item className={classes.hodeOnXS}>
        <Typography align="center" color="textPrimary" component="p">
          BeautyCareLondon
        </Typography>
      </Grid>
    </>
  );
}
