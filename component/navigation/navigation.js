// react
import React, { useState, useEffect, useLayoutEffect } from "react";
// next
import { useRouter } from "next/router";
// mui
import {
  Grid,
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  InputBase,
  Divider,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import { makeStyles, fade, useTheme } from "@material-ui/core/styles";

// component
import PrimaryNavigation from "./primaryNavigation";
import SecondaryNavigation from "./secondaryNavigation";
import Search from "./search";

const useStyles = makeStyles((theme) => ({
  root: {},
  appbar: {
    paddingTop: theme.spacing(1),
  },
  toolbar: {},
}));

export default function Navigation() {
  const classes = useStyles();
  const theme = useTheme();

  // We don't want to display Contact and Secondary Navigation
  // When we are in Blogs page
  const router = useRouter();
  const weareinBlogs = router.asPath === "/blogs";
  const weareinContact = router.asPath === "/contact";

  // ======================= Second Iteration ========================
  // // TODO: This will be added to add search bar in the second iteration
  // const [extendedSearch, setExtendedSearch] = useState(false);
  //
  // const handleSeacrhWidth = () => {
  //   const viewportWidth = window.innerWidth;
  //   viewportWidth >= theme.breakpoints.values["md"]
  //     ? setExtendedSearch(false)
  //     : setExtendedSearch(true);
  // };
  //
  // useEffect(() => {
  //   window.addEventListener("resize", handleSeacrhWidth);
  //   handleSeacrhWidth();
  //   return () => window.removeEventListener("resize", handleSeacrhWidth);
  // }, []);

  // ======================= Second Iteration ========================

  return (
    <Grid container item xs={12} className={classes.root}>
      <AppBar
        position="static"
        color="transparent"
        variant="elevation"
        className={classes.appbar}
      >
        <Toolbar className={classes.toolbar} variant="dense">
          <Grid
            item
            container
            direction="column"
            xs={12}
            alignItems="stretch"
            justify="flex-start"
            spacing={1}
          >
            <PrimaryNavigation />
            {weareinBlogs || weareinContact ? null : <SecondaryNavigation />}
          </Grid>
        </Toolbar>
      </AppBar>
    </Grid>
  );
}
