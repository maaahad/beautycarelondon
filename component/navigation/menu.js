// react
import React, { useState, useReducer } from "react";

// react-icons
import { FaGifts, FaMale, FaFemale, FaChild } from "react-icons/fa";

// clsx
import clsx from "clsx";

// next
import Link from "next/link";
// data
import { productMenus } from "./secondaryNavigation";

// mui
import {
  Grid,
  IconButton,
  Menu,
  MenuItem,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  Typography,
  ListSubheader,
  Collapse,
  Hidden,
  Avatar,
  Button,
  Tooltip,
  Fab,
} from "@material-ui/core";
import { deepOrange } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import PersonIcon from "@material-ui/icons/Person";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import HomeIcon from "@material-ui/icons/Home";
import ContactPhoneIcon from "@material-ui/icons/ContactPhone";
import NoteIcon from "@material-ui/icons/Note";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

// styles
const useStyles = makeStyles((theme) => ({
  drawer: {
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  fixedWidthDrawer: {
    width: 250,
  },
  autoWidthDrawer: {
    width: "auto",
  },
  avatar: {
    width: theme.spacing(2),
    height: theme.spacing(2),
  },
  orange: {
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: deepOrange[500],
  },
  leftnav: {
    padding: theme.spacing(3, 0),
    display: "flex",
    flexFlow: "row nowrap",
    alignItems: "center",
    justifyContent: "center",
    "& > * + *": {
      marginLeft: theme.spacing(3),
    },
    "& > *": {
      "&:hover": {
        // color: theme.palette.secondary.main,
      },
    },
  },
  iconButton: {
    "&:hover": {
      color: theme.palette.secondary.main,
    },
  },
  search: {
    padding: theme.spacing(2),
  },
  productCategory: {
    "&:hover": {
      color: theme.palette.secondary.main,
    },
  },
  user: {
    padding: theme.spacing(2),
    display: "flex",
    flexFlow: "row nowrap",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  logout: {},
}));

export default function BCLMenu({ anchor = "left" }) {
  const classes = useStyles();

  const [open, toggleOpen] = useReducer((open) => !open, false);

  const toggleDrawer = (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    toggleOpen();
  };

  const menus = (
    <div
      className={clsx(classes.drawer, classes.fixedWidthDrawer, {
        [classes.autoWidthDrawer]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
      onClick={toggleDrawer}
      onKeyDown={toggleDrawer}
    >
      {/* Leftnav */}
      <div className={clsx(classes.leftnav)}>
        <Link href="/" passHref>
          <Tooltip title="Home" aria-label="home">
            <Fab size="small" color="primary">
              <HomeIcon />
            </Fab>
          </Tooltip>
        </Link>
        <Link href="/blogs" passHref>
          <Tooltip title="Blogs" aria-label="blogs">
            <Fab size="small" color="primary">
              <NoteIcon />
            </Fab>
          </Tooltip>
        </Link>
        <Link href="/contact" passHref>
          <Tooltip title="Contact" aria-label="contact">
            <Fab size="small" color="primary">
              <ContactPhoneIcon />
            </Fab>
          </Tooltip>
        </Link>
      </div>

      <List
        component="nav"
        aria-labelledby="product-categories"
        subheader={
          <>
            <ListSubheader id="product-categories" component="div">
              Products
            </ListSubheader>
            <Divider />
          </>
        }
        dense
      >
        {productMenus.map((productMenu, key) => (
          <ListItem
            key={key}
            button
            focusRipple
            dense
            className={classes.productCategory}
          >
            <Link href={productMenu.href} passHref>
              <ListItemText primary={productMenu.name} />
            </Link>
          </ListItem>
        ))}
      </List>
      <Divider />
      <List component="nav" aria-labelledby="user" dense>
        <ListItem button focusRipple dense>
          <ListItemIcon>
            <PersonIcon color="primary" />
          </ListItemIcon>
          <ListItemText primary={"username"} />
        </ListItem>
        <ListItem button focusRipple dense className={classes.logout}>
          <ListItemIcon>
            <ExitToAppIcon color="primary" />
          </ListItemIcon>
          <ListItemText primary={"Logout/Login"} />
        </ListItem>
      </List>

      {/* // TODO: This will be hidden for now ...  */}
      <Hidden xsUp implementation="css">
        <Divider />
        <List
          component="nav"
          aria-labelledby="user"
          subheader={
            <ListSubheader id="user" component="div">
              User
            </ListSubheader>
          }
          dense
        >
          <ListItem button focusRipple>
            <ListItemIcon>
              <PersonIcon />
            </ListItemIcon>
            <ListItemText primary="maaahad" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <ExitToAppIcon />
            </ListItemIcon>
            <ListItemText primary="logout" />
          </ListItem>
        </List>
      </Hidden>
    </div>
  );

  return (
    <Grid item>
      <IconButton
        size="medium"
        edge="start"
        color="textPrimary"
        aria-label="menu"
        aria-haspopup={true}
        onClick={toggleDrawer}
        className={classes.iconButton}
      >
        <MenuIcon />
      </IconButton>
      <Drawer
        anchor={anchor}
        open={open}
        onClose={toggleDrawer}
        variant="temporary"
      >
        {menus}
      </Drawer>
    </Grid>
  );
}
