// React
import React from "react";

// Next
import Link from "next/link";
import { useRouter } from "next/router";
// mui
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// component

// references
// wrap Button from mui with Link from next
//      Reference: https://dev.to/ivandotv/using-next-js-link-component-with-material-ui-buttons-and-menu-items-3m6a

// css
const useStyles = makeStyles((theme) => ({
  link: ({ router, href }) => {
    const activeStyle =
      router.asPath === href
        ? {
            color: theme.palette.grey[500],
            borderBottom: `2px solid ${theme.palette.common.black}`,
          }
        : {
            color: theme.palette.common.black,
          };
    return {
      paddingRight: theme.spacing(2),
      paddingLeft: theme.spacing(2),
      borderRadius: "0",
      borderBottom: "2px solid transparent",
      "&:hover": {
        backgroundColor: "transparent",
        borderBottom: `2px solid ${theme.palette.common.black}`,
      },
      ...activeStyle,
    };
  },
}));

export default function ActiveableMenuLink({
  href,
  label,
  size = "medium",
  disableRipple = false,
  onMenuExpand = (f) => f,
}) {
  const router = useRouter();
  const classes = useStyles({ router, href });

  // TODO:       onMouseLeave={onMenuExpand(label, true)}
  return (
    <Button
      size={size}
      disableRipple={disableRipple}
      className={classes.link}
      onMouseOver={onMenuExpand(label, true)}
    >
      {label}
    </Button>
  );
}
