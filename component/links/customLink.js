// react

// next
import Link from "next/link";
// mui

// NOTE: Deprecated.....
export default function CustomLink({ href, Child, funcArgs, classes }) {
  return (
    <Link href={href} passHref>
      <Child productMenu={funcArgs} classes={classes} />
    </Link>
  );
}
