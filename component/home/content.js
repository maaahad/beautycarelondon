// react

// next

// mui
import { Grid, Divider } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// components
import Hero from "./hero";
import Deals from "./deals";
import NewArrivals from "./newArrivals";
import BestSellers from "./bestSellers";

// css
const useStyles = makeStyles((theme) => ({
  root: {
    "& > * + *": {
      marginTop: theme.spacing(6),
    },
  },
}));

export default function Content() {
  const classes = useStyles();
  return (
    <Grid
      item
      container
      xs={12}
      direction="column"
      alignItems="stretch"
      justify="flex-start"
      className={classes.root}
    >
      <Hero />
      <Deals />
      <NewArrivals />
      <BestSellers />
    </Grid>
  );
}
