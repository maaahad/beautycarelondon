// React
import React, { useState, useEffect } from "react";
// next
import Image from "next/image";

// clsx
import clsx from "clsx";

// mui
import { Grid, Paper, Button } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import ArrowForwardIosSharpIcon from "@material-ui/icons/ArrowForwardIosSharp";
import ArrowBackIosSharpIcon from "@material-ui/icons/ArrowBackIosSharp";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import { makeStyles, useTheme } from "@material-ui/core/styles";

// carousel
import Carousel from "react-material-ui-carousel";

// css

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    // height: 200,
    width: "100vw",
    height: "calc(100vh - 200px)",
  },
}));

const imageUrls = [
  "/images/home/hero/hero1.jpg",
  "/images/home/hero/hero2.jpg",
  "/images/home/hero/hero3.jpg",
  "/images/home/hero/hero4.jpg",
  "/images/home/hero/hero5.jpg",
  "/images/home/hero/hero6.jpg",
];

function CarouselItem({ imageUrl }) {
  const classes = useStyles();
  return <img src={imageUrl} className={classes.image} />;
}

export default function Hero() {
  const classes = useStyles();
  const theme = useTheme();
  const prevIcon = (
    <IconButton aria-label="previous image">
      <ArrowBackIosSharpIcon color="primary" />
    </IconButton>
  );
  const nextIcon = (
    <IconButton aria-label="previous image">
      <ArrowForwardIosSharpIcon color="primary" />
    </IconButton>
  );
  return (
    <Grid item xs={12} className={classes.root}>
      <Carousel
        animation="slide"
        autoPlay
        NextIcon={nextIcon}
        PrevIcon={prevIcon}
        fullHeightHover={true}
        navButtonsProps={{
          style: {
            // backgroundColor: theme.palette.grey[600],
            borderRadius: "50%",
            color: theme.palette.primary.main,
            opacity: 0.5,
            width: 50,
            height: 50,
          },
        }}
        navButtonsWrapperProps={{
          style: {
            // backgroundColor: "red",
          },
        }}
      >
        {imageUrls.map((imageUrl, i) => (
          <CarouselItem key={i} imageUrl={imageUrl} />
        ))}
      </Carousel>
    </Grid>
  );
}

// const useStyles = makeStyles((theme) => ({
//   root: {},
//   hero: {
//     height: 500,
//     padding: theme.spacing(2),
//   },
//   carousel: {
//     height: 400,
//     transition: "all 5s ease-in",
//   },
//   arrow: {
//     display: "flex",
//     alignItems: "center",
//     "&:hover": {
//       "& > *": {
//         color: theme.palette.primary.main,
//       },
//     },
//   },
//   left_arrow: {
//     justifyContent: "flex-start",
//   },
//   image_container: {
//     display: "flex",
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   image: {
//     height: 400,
//     // width: "100%",
//   },
//   right_arrow: {
//     justifyContent: "flex-end",
//   },
//   dots: {
//     height: 100,
//     "& > div + div": {
//       marginLeft: theme.spacing(2),
//     },
//   },
//   dot: {
//     width: 8,
//     height: 8,
//     borderRadius: "25%",
//     backgroundColor: theme.palette.grey[400],
//     "&:hover": {
//       backgroundColor: theme.palette.grey[600],
//       cursor: "pointer",
//     },
//   },
//   active_dot: {
//     backgroundColor: theme.palette.primary.main,
//   },
// }));
//
//
//
// const useImage = (imageUrls) => {
//   const [index, setIndex] = useState(0);
//   const imageUrlsLength = imageUrls.length;
//
//   const nextImageUrl = () => {
//     if (index === imageUrlsLength - 1) {
//       setIndex(0);
//     } else {
//       setIndex(index + 1);
//     }
//   };
//
//   const prevImageUrl = () => {
//     if (index === 0) {
//       setIndex(imageUrlsLength - 1);
//     } else {
//       setIndex(index - 1);
//     }
//   };
//
//   const setImage = (urlIndex) => {
//     if (urlIndex >= 0 || urlIndex < imageUrlsLength) {
//       setIndex(urlIndex);
//     } else {
//       setIndex(0);
//     }
//   };
//
//   // TODO: setInterval not working properly....
//   // setInterval(() => setImage(index + 1), 5000);
//
//   return {
//     urlIndex: index,
//     imageUrl: imageUrls[index],
//     prevImageUrl,
//     nextImageUrl,
//     setImage,
//   };
// };

// export default function Hero() {
//   const classes = useStyles();
//   const { urlIndex, imageUrl, prevImageUrl, nextImageUrl, setImage } = useImage(
//     imageUrls
//   );
//
//   return (
//     <Grid
//       item
//       container
//       direction="column"
//       xs={12}
//       alignItems="stretch"
//       justify="flex-start"
//       className={classes.hero}
//       classes={{ root: classes.root }}
//     >
//       <Grid
//         item
//         container
//         direction="row"
//         alignItems="center"
//         justify="space-between"
//         className={classes.carousel}
//       >
//         <Grid item xs={2} className={clsx(classes.arrow, classes.left_arrow)}>
//           <IconButton aria-label="previous image" onClick={prevImageUrl}>
//             <ArrowBackIosSharpIcon />
//           </IconButton>
//         </Grid>
//         <Grid item xs={8} className={classes.image_container}>
//           <img src={imageUrl} alt="hero image" className={classes.image} />
//         </Grid>
//         <Grid item xs={2} className={clsx(classes.arrow, classes.right_arrow)}>
//           <IconButton aria-label="next image" onClick={nextImageUrl}>
//             <ArrowForwardIosSharpIcon />
//           </IconButton>
//         </Grid>
//       </Grid>
//       <Grid
//         item
//         container
//         direction="row"
//         alignItems="center"
//         justify="center"
//         className={classes.dots}
//       >
//         {imageUrls.map((iu, i) => (
//           <div
//             key={i}
//             className={clsx(classes.dot, i === urlIndex && classes.active_dot)}
//             onClick={() => setImage(i)}
//           />
//         ))}
//       </Grid>
//     </Grid>
//   );
// }
