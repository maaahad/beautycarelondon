// React

// next

// mui
import { Grid } from "@material-ui/core";

// components
import ProductCardList from "../products/productCardList";

// css

export default function BestSellers() {
  return <ProductCardList title="Best Sellers" more_text="More Products" />;
}
