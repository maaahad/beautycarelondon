// React

// next

// mui

// components
import ProductCardList from "../products/productCardList";

// css

export default function NewArrivals() {
  return <ProductCardList title="New Arrivals" more_text="More Products" />;
}
