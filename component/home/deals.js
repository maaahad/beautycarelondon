// React

// next

// mui
import { makeStyles } from "@material-ui/core/styles";

// components
import ProductCardList from "../products/productCardList";

// css
const useStyles = makeStyles((theme) => ({}));

export default function Deals() {
  const classes = useStyles();
  return <ProductCardList title="Deals" more_text="More Deals" />;
}
