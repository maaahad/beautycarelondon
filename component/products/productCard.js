// react

// mui
import {
  Grid,
  Divider,
  Typography,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  Avatar,
  IconButton,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";

// next
import Link from "next/link";
// css
const useStyles = makeStyles((theme) => ({
  card: {
    // backgroundColor: "red",
  },
  media: {
    height: 0,
    paddingTop: "56.25%",
  },
  price: {
    marginTop: theme.spacing(1),
    display: "flex",
    flexFlow: "row wrap",
    alignItems: "center",
    justifyContent: "flex-start",
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
    "& > *": {
      fontSize: theme.typography.htmlFontSize,
      fontWeight: theme.typography.fontWeightMedium,
    },
  },
  price_before_deal: (deal) => {
    return deal
      ? {
          textDecorationLine: "line-through",
          textDecorationStyle: "solid",
          textDecorationColor: theme.palette.secondary.main,
        }
      : {};
  },
  price_after_deal: {},
  actions: {
    justifyContent: "space-between",
  },
  order: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  learn_more: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

export default function ProductCard({
  product,
  productUrl = "/products/TEST_PRODUCT",
  deal = true,
}) {
  const classes = useStyles(deal);
  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="product" className={classes.product_initial}>
            P
          </Avatar>
        }
        action={
          <IconButton aria-label="share">
            <ShareIcon />
          </IconButton>
        }
        title="Product Name"
        subheader="product subtitle"
      />
      <CardMedia
        className={classes.media}
        image="/images/home/hero/hero2.jpg"
        title="Product name"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          This is a brief description of the product
        </Typography>
        <div className={classes.price}>
          <Typography
            variant="overline"
            color="primary"
            component="p"
            className={classes.price_before_deal}
          >
            700 TK
          </Typography>
          {deal && (
            <Typography
              variant="overline"
              color="primary"
              component="p"
              className={classes.price_after_deal}
            >
              600 TK
            </Typography>
          )}
        </div>
      </CardContent>
      <CardActions className={classes.actions}>
        <Grid
          container
          direction="row"
          alignItems="center"
          justify="space-between"
          spacing={1}
        >
          <Grid item xs={4}>
            <IconButton aria-label="favourite">
              <FavoriteIcon />
            </IconButton>
          </Grid>
          <Grid item xs={4}>
            {/*TODO: Add the href later to the order page*/}
            <Link href={"#"} passHref>
              <Button
                variant="outlined"
                size="small"
                color="primary"
                className={classes.order}
              >
                Order
              </Button>
            </Link>
          </Grid>
          <Grid item xs={4}>
            <Link href={productUrl} passHref>
              <Button
                variant="outlined"
                size="small"
                color="primary"
                className={classes.learn_more}
              >
                Learn More
              </Button>
            </Link>
          </Grid>
        </Grid>
      </CardActions>
    </Card>
  );
}
