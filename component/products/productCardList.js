// react

// next
import Link from "next/link";

// mui
import {
  Grid,
  Divider,
  Typography,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  Avatar,
  IconButton,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";

// components
import ProductCard from "./productCard";

// css
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  header: {
    // backgroundColor: "green",
  },
  header_text: {
    textTransform: "uppercase",
    fontSize: theme.typography.fontSize,
    fontWeight: theme.typography.fontWeightMedium,
  },
  bar: {
    "& > div": {
      width: "80vw",
      height: 1,
      backgroundColor: theme.palette.grey[400],
    },
  },
  cards: {
    padding: theme.spacing(2),
  },
  more: {
    marginTop: theme.spacing(4),
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

export default function ProductCardList({ products, title, more_text }) {
  const classes = useStyles();
  return (
    <Grid
      item
      container
      xs={12}
      className={classes.root}
      direction="column"
      alignItems="stretch"
      justify="flex-start"
    >
      <Grid
        container
        item
        xs={12}
        direction="column"
        alignItems="center"
        justify="flex-start"
        className={classes.header}
      >
        <Grid item className={classes.bar}>
          <div></div>
        </Grid>
        <Grid item>
          <Typography
            variant="overline"
            component="p"
            align="center"
            color="secondary"
            className={classes.header_text}
          >
            {title}
          </Typography>
        </Grid>
        <Grid item className={classes.bar}>
          <div></div>
        </Grid>
      </Grid>
      <Grid item container className={classes.cards} spacing={4}>
        <Grid item xs={12} sm={6} md={4} lg={3} xl={2} className={classes.card}>
          <ProductCard />
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={3} xl={2} className={classes.card}>
          <ProductCard />
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={3} xl={2} className={classes.card}>
          <ProductCard />
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={3} xl={2} className={classes.card}>
          <ProductCard />
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={3} xl={2} className={classes.card}>
          <ProductCard />
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={3} xl={2} className={classes.card}>
          <ProductCard />
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={3} xl={2} className={classes.card}>
          <ProductCard />
        </Grid>
      </Grid>
      <Grid item xs={12} className={classes.more}>
        <Link href="/deals" passHref>
          <Button variant="contained" size="small" color="primary">
            {more_text}
          </Button>
        </Link>
      </Grid>
    </Grid>
  );
}
