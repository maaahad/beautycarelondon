// react
import React from "react";
// next
import Head from "next/head";
// mui
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// components
import Header from "./headers/header";

// inspired website : https://www.michaelkors.co.uk/

const useStyles = makeStyles((theme) => ({
  root: {},
  header: {},
  main: {
    backgroundColor: "blue",
    height: 1000,
    marginTop: 60,
  },
  footer: {
    backgroundColor: "green",
    height: 1000,
  },
}));

export default function Layout({ children }) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Head>
        <title>BeatyCareLondon</title>
        <meta
          name="description"
          content="London based original beauty care products"
        />
      </Head>
      <Grid
        container
        direction="column"
        alignItems="stretch"
        justify="flex-start"
      >
        <Grid item xs={12} className={classes.header}>
          <Header />
        </Grid>
        <Grid item xs={12}>
          {/*children*/}
          <div className={classes.main}>This is Main</div>
        </Grid>
        <Grid item xs={12}>
          <div className={classes.footer}>This is footer</div>
        </Grid>
      </Grid>
    </div>
  );
}

// // component
// import Navigation from "./navigation/navigation";
// import Footer from "./footer";
//
// // css
//
// const useStyles = makeStyles((theme) => ({
//   root: {},
//   footer: {
//     backgroundColor: "red",
//   },
// }));
//
// export default function Layout({ children }) {
//   const classes = useStyles();
//   return (
//     <div className={classes.root}>
//       <Head>
//         <title>BeautyCareLondon</title>
//         <meta
//           name="description"
//           content="London based original beauty care products"
//         />
//       </Head>
//
//       <Grid
//         container
//         direction="column"
//         alignItems="stretch"
//         justify="flex-start"
//         spacing={2}
//       >
//         <Navigation />
//         {children}
//         <Footer />
//       </Grid>
//     </div>
//   );
// }
