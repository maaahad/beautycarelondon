// react

// next
// mui
import { Grid, Typography, Link } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import CopyrightIcon from "@material-ui/icons/Copyright";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";

// component
import SecondaryNavigation from "./navigation/secondaryNavigation";

// css
const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    backgroundColor: theme.palette.grey[200],
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  message: {},
  social_links: {
    display: "flex",
    flexFlow: "row nowrap",
    alignItems: "center",
    justifyContent: "center",
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
  },
  copyright: {
    textAlign: "center",
  },
  copyright_first_line: {
    display: "flex",
    flexFlow: "row nowrap",
    alignItems: "center",
    justifyContent: "center",
    "& > * + *": {
      marginLeft: theme.spacing(0.5),
    },
  },
  developer: {
    alignSelf: "flex-end",
  },
  overline: {
    fontSize: ".6rem",
  },
}));

export default function Footer() {
  const classes = useStyles();
  return (
    <Grid
      item
      container
      direction="column"
      alignItems="center"
      justify="flex-start"
      className={classes.root}
    >
      <Grid item className={classes.message}>
        <Typography variant="overline" display="block" color="secondary">
          A message to the customer from the seller ...
        </Typography>
      </Grid>
      <Grid item>
        <SecondaryNavigation divider={false} />
      </Grid>
      <Grid item className={classes.social_links}>
        <a href="https://www.facebook.com/beautycarelondonn" target="_blank">
          <FacebookIcon color="primary" />
        </a>
        {/*
        <a href="#" target="_blank">
          <TwitterIcon color="primary" />
        </a>
        */}
      </Grid>
      <Grid item className={classes.copyright}>
        <div className={classes.copyright_first_line}>
          <CopyrightIcon color="primary" />
          <Typography>BeautyCareLondon, 2021.</Typography>
        </div>
        <Typography>All Rights Reserved.</Typography>
      </Grid>
      <Grid item className={classes.developer}>
        <Typography
          variant="overline"
          display="block"
          classes={{ overline: classes.overline }}
        >
          <Link href="https://muhammedahad.netlify.app/" target="_blank">
            Site Developed by: Muhammed Ahad
          </Link>
        </Typography>
      </Grid>
    </Grid>
  );
}
