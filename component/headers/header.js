// react
import React, { useState, useEffect, useRef } from "react";
// next

// mui
import { Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";

// component
import Nav from "./nav";

const useStyles = makeStyles((theme) => ({
  root: {},
  promo: {
    height: 35,
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  overline: {
    fontSize: 12,
  },
  navigation: {
    // width: "100%",
    // backgroundColor: "red",
  },
  code: {
    borderBottom: `1px solid ${theme.palette.common.white}`,
  },
}));

export default function Header() {
  const classes = useStyles();
  const promoRef = useRef(null);
  const navRef = useRef(null);

  const handleWindowScroll = (event) => {
    const scrollY = window.scrollY;
    if (scrollY >= 35) {
      // promoRef.current.style.display = "none";
      navRef.current.style.position = "fixed";
      navRef.current.style.top = "0";
      navRef.current.style.left = "0";
    } else {
      // promoRef.current.style.display = "flex";
      navRef.current.style.position = "static";
      navRef.current.style.width = "100%";
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleWindowScroll);
    return () => window.removeEventListener("scroll", handleWindowScroll);
  }, []);
  return (
    <header className={classes.header}>
      <div className={classes.root}>
        <div ref={promoRef} className={classes.promo}>
          <Typography
            variant="overline"
            classes={{ overline: classes.overline }}
          >
            Use the code <strong className={classes.code}>maaahad</strong> to
            get <strong>5%</strong> discount
          </Typography>
        </div>
        <Nav ref={navRef} />
      </div>
    </header>
  );
}
