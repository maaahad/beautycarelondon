// React
import React from "react";
// next
// mui
import { Grid, Button, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";

// component
import ActiveableMenuLink from "../links/activeableMenuLink";

// lib
import { menus } from "../../lib/bcl";

// css
const useStyles = makeStyles((theme) => ({
  menus: {
    marginTop: theme.spacing(2),
    // marginBottom: theme.spacing(1),
    display: "flex",
    flexFlow: "row nowrap",
    alignItems: "center",
    justifyContent: "center",
    "& > * + *": {
      marginLeft: theme.spacing(4),
    },
  },
}));

export default function HorizontalMenu({ setExpandMenu }) {
  const classes = useStyles();
  const onMenuExpand = (menu, expand) => (event) => {
    console.log("Clicked : ", menu);
    setExpandMenu(expand);
  };
  return (
    <Grid item className={classes.menus}>
      {menus.map((menu, index) => (
        <ActiveableMenuLink
          key={index}
          href={menu.href}
          label={menu.label}
          size="small"
          disableRipple={true}
          onMenuExpand={onMenuExpand}
        />
      ))}
    </Grid>
  );
}
