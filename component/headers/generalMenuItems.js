// react

// clsx
import clsx from "clsx";

// mui
import {
  Grid,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// css
const useStyles = makeStyles((theme) => ({
  root: {},
  category: {
    fontSize: ".8rem",
    fontWeight: theme.typography.fontWeightMedium,
    letterSpacing: ".1rem",
    textTransform: "capitalize",
    "&:hover": {
      borderBottom: "1px solid",
      cursor: "pointer",
    },
  },
  product: {
    color: theme.palette.grey[800],
    fontSize: ".7rem",
    fontWeight: theme.typography.fontWeightRegular,
    letterSpacing: ".08rem",
  },
}));

export default function GeneralMenuItems({ category }) {
  const classes = useStyles();
  return (
    <Grid item xs={3}>
      <List
        dense
        component="nav"
        aria-labelledby="product-group"
        subheader={
          <ListSubheader>
            <Typography
              variant="caption"
              color="textPrimary"
              className={classes.category}
            >
              {category}
            </Typography>
          </ListSubheader>
        }
      >
        <ListItem dense button>
          <ListItemText
            primary={
              <Typography variant="caption" className={classes.product}>
                Shorts
              </Typography>
            }
          />
        </ListItem>
        <ListItem dense button>
          <ListItemText
            primary={
              <Typography variant="caption" className={classes.product}>
                Shorts
              </Typography>
            }
          />
        </ListItem>
        <ListItem dense button>
          <ListItemText
            primary={
              <Typography variant="caption" className={classes.product}>
                Shorts
              </Typography>
            }
          />
        </ListItem>
      </List>
    </Grid>
  );
}
