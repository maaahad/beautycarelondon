// React

// mui
import {
  Grid,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  Typography,
  Paper,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// css

const useStyles = makeStyles((theme) => ({
  root: {},
  offers: {
    color: theme.palette.secondary.main,
    fontSize: ".8rem",
    fontWeight: theme.typography.fontWeightMedium,
    letterSpacing: ".1rem",
    textTransform: "capitalize",
    "&:hover": {
      borderBottom: "1px solid",
    },
  },
  special_product: {
    color: theme.palette.grey[800],
    fontSize: ".70rem",
    fontWeight: theme.typography.fontWeightRegular,
    letterSpacing: ".08rem",
  },
}));

export default function SpecialMenuItems() {
  const classes = useStyles();

  return (
    <div item className={classes.root}>
      <Paper elevation="0" square>
        <List
          dense
          component="nav"
          aria-labelledby="product-group"
          subheader={
            <ListSubheader>
              <Typography
                variant="caption"
                color="textPrimary"
                className={classes.offers}
              >
                Specially For you
              </Typography>
            </ListSubheader>
          }
        >
          <ListItem dense button>
            <ListItemText
              primary={
                <Typography
                  variant="caption"
                  className={classes.special_product}
                >
                  Bestsellers
                </Typography>
              }
            />
          </ListItem>
          <ListItem dense button>
            <ListItemText
              primary={
                <Typography
                  variant="caption"
                  className={classes.special_product}
                >
                  New Arrivals
                </Typography>
              }
            />
          </ListItem>
          <ListItem dense button>
            <ListItemText
              primary={
                <Typography
                  variant="caption"
                  className={classes.special_product}
                >
                  On Sale
                </Typography>
              }
            />
          </ListItem>
        </List>
      </Paper>
    </div>
  );
}
