// react

// next

// mui
import {
  Grid,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  Divider,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// component
import GeneralMenuItems from "./generalMenuItems";
import SpecialMenuItems from "./specialMenuItems";

// css
const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
  },
  special_menu_items: {},
}));

export default function ExpandedMenu() {
  const classes = useStyles();
  return (
    <Grid
      item
      container
      direction="row"
      alignItems="flex-start"
      justify="space-between"
      className={classes.root}
    >
      <Grid
        item
        md={9}
        container
        direction="row"
        alignItems="flex-start"
        justify="space-between"
      >
        <GeneralMenuItems category="Clothing" />
        <GeneralMenuItems category="Perfume" />
        <GeneralMenuItems category="Bags" />
        <GeneralMenuItems category="Bodycare" />
        <GeneralMenuItems category="Watches" />
        <GeneralMenuItems category="Shoes" />
        <GeneralMenuItems category="Accessories" />
        <GeneralMenuItems category="All" />
      </Grid>
      <Grid item md={3} className={classes.special_menu_items}>
        <SpecialMenuItems />
      </Grid>
    </Grid>
  );
}
