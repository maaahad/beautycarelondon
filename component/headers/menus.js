// react
import React, { useState } from "react";
// next
import Link from "next/link";

// mui
import {
  Grid,
  SwipeableDrawer,
  List,
  ListSubheader,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  Typography,
  IconButton,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import MenuOutlinedIcon from "@material-ui/icons/MenuOutlined";
import HomeIcon from "@material-ui/icons/Home";
import LocalMallOutlinedIcon from "@material-ui/icons/LocalMallOutlined";
import FacebookIcon from "@material-ui/icons/Facebook";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
// lib
import { menus } from "../../lib/bcl";

// css

const useStyles = makeStyles((theme) => ({
  icon_button: {
    color: theme.palette.common.black,
    "&:hover": {
      backgroundColor: "transparent",
      color: theme.palette.grey[600],
    },
  },
  stuff_list: {
    width: 250,
  },
  shortcut_menu: {
    padding: theme.spacing(1, 2),
    display: "flex",
    flexFlow: "row nowrap",
    alignItems: "center",
    justifyContent: "center",
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
  },
  follow: {
    padding: theme.spacing(1, 2),
    display: "flex",
    flexFlow: "row nowrap",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  facebook_icon: {
    color: theme.palette.common.black,
    "&:hover": {
      backgroundColor: "transparent",
      color: theme.palette.primary.main,
    },
  },
  user: {
    padding: theme.spacing(1, 2),
    display: "flex",
    flexFlow: "column wrap",
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  text_button: {
    color: theme.palette.common.black,
    "&:hover": {
      backgroundColor: "transparent",
      color: theme.palette.grey[600],
    },
  },
}));

function DrawerStuff({ toggleDrawer }) {
  const classes = useStyles();
  return (
    <div
      role="presentation"
      onClick={toggleDrawer(false)}
      onKeyDown={toggleDrawer(false)}
      className={classes.stuff_list}
    >
      <div className={classes.shortcut_menu}>
        <IconButton
          disableRipple
          classes={{ root: classes.icon_button }}
          size="medium"
          aria-label="Go To Home"
        >
          <Link href="/" passHref>
            <HomeIcon />
          </Link>
        </IconButton>
        <IconButton
          disableRipple
          classes={{ root: classes.icon_button }}
          size="medium"
          aria-label="Go To Your Basket"
        >
          <Link href="/" passHref>
            <LocalMallOutlinedIcon />
          </Link>
        </IconButton>
      </div>
      <Divider />
      <List
        component="nav"
        aria-labelledby="menus-in-drawer"
        subheader={
          <ListSubheader component="div" id="menus-in-drawer">
            Browse for Products
          </ListSubheader>
        }
        dense
      >
        {menus.map((menu, i) => (
          <ListItem button key={i}>
            <Link href={menu.href} passHref>
              <ListItemText
                primary={<Typography variant="button">{menu.label}</Typography>}
                inset
              />
            </Link>
          </ListItem>
        ))}
      </List>
      <Divider />
      <div className={classes.user}>
        {/*// TODO: Add Link from NextJS*/}
        <Button
          variant="text"
          startIcon={<PersonAddIcon />}
          classes={{ text: classes.text_button }}
        >
          Sign in
        </Button>
        <Button
          variant="text"
          startIcon={<ExitToAppIcon />}
          classes={{ text: classes.text_button }}
        >
          Sign Out
        </Button>
      </div>
      <Divider />
      <div className={classes.follow}>
        <Typography variant="overline">Follow us on :</Typography>
        <IconButton
          disableRipple
          classes={{ root: classes.facebook_icon }}
          color="primary"
          size="medium"
          aria-label="Facebook Page"
        >
          <Link href="https://www.facebook.com/beautycarelondonn" passHref>
            <FacebookIcon />
          </Link>
        </IconButton>
      </div>
    </div>
  );
}

export default function Menus({ anchor = "left" }) {
  const [drawer, setDrawer] = useState(false);
  const toggleDrawer = (open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setDrawer(open);
  };
  const classes = useStyles();
  return (
    <Grid item xs={4}>
      <IconButton
        size="small"
        disableRipple
        classes={{ root: classes.icon_button }}
        onClick={toggleDrawer(true)}
      >
        <MenuOutlinedIcon />
      </IconButton>
      <SwipeableDrawer
        anchor={anchor}
        open={drawer}
        onOpen={toggleDrawer(true)}
        onClose={toggleDrawer(false)}
      >
        <DrawerStuff toggleDrawer={toggleDrawer} />
      </SwipeableDrawer>
    </Grid>
  );
}
