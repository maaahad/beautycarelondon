// React
import React from "react";
// next
// mui
import { Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  brand: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

// TODO: will generalize it later

export default function Brand({ text = "BeautyCareLondon" }) {
  const classes = useStyles();
  return (
    <Grid item className={classes.brand}>
      <Typography variant="h6">{text}</Typography>
    </Grid>
  );
}
