// React
import React, { useState } from "react";
// next
// mui
import { Grid, AppBar, Toolbar, Typography, Hidden } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";

// component
import TopNav from "./topnav";
import HorizontalMenu from "./horizontalMenu";
import ExpandedMenu from "./expandedMenu";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.common.white,
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(0.5),
    },
  },
}));

const Nav = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const [expandMenu, setExpandMenu] = useState(false);
  return (
    <AppBar
      ref={ref}
      position="static"
      color="transparent"
      variant="outlined"
      className={classes.appbar}
      classes={{ root: classes.root }}
    >
      <Toolbar variant="dense" className={classes.toolbar}>
        <Grid
          container
          direction="column"
          alignItems="stretch"
          justify="flex-start"
          className={classes.nav}
        >
          <TopNav />
          <Hidden smDown>
            <HorizontalMenu setExpandMenu={setExpandMenu} />
            {expandMenu && <ExpandedMenu />}
          </Hidden>
        </Grid>
      </Toolbar>
    </AppBar>
  );
});

export default Nav;
