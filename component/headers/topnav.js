// React
import React, { useState, useReducer } from "react";
// next
import Link from "next/link";
// mui
import { Grid, Typography, Hidden, InputBase, Drawer } from "@material-ui/core";
import { fade, makeStyles } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import PersonOutlineOutlinedIcon from "@material-ui/icons/PersonOutlineOutlined";
import SearchOutlinedIcon from "@material-ui/icons/SearchOutlined";
import FavoriteBorderOutlinedIcon from "@material-ui/icons/FavoriteBorderOutlined";
import LocalMallOutlinedIcon from "@material-ui/icons/LocalMallOutlined";
import MenuOutlinedIcon from "@material-ui/icons/MenuOutlined";
import SearchIcon from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close";

// component
import Menus from "./menus";

const useStyles = makeStyles((theme) => ({
  topnav: {
    height: 35,
  },
  country: {
    display: "flex",
    flexFlow: "row nowrap",
    alignItems: "center",
    justifyContent: "flex-start",
    "& > * + *": {
      marginLeft: theme.spacing(0.5),
    },
    "& > img:nth-of-type(1)": {
      width: 30,
    },
    "& > img:nth-of-type(2)": {
      width: 8,
    },
  },
  brand: (displaySearchBar) => ({
    display: "flex",
    alignItems: "center",
    justifyContent: displaySearchBar ? "flex-start" : "center",
    "&:hover": {
      cursor: "pointer",
    },
  }),
  search: {
    width: "100%",
    // height: "100%",
    height: 30,
    position: "relative",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    borderRadius: theme.shape.borderRadius,
    border: `1px solid ${theme.palette.grey[300]}`,
    // backgroundColor: fade(theme.palette.common.white, 0.15),
    // backgroundColor: theme.palette.grey[100],
    "&:hover": {
      // backgroundColor: fade(theme.palette.common.white, 0.25),
      backgroundColor: theme.palette.grey[100],
    },

    margin: theme.spacing(0, 2),
  },
  search_icon: {
    padding: theme.spacing(0, 1),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  input_root: {
    color: "inherit",
  },
  input_input: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(3)}px)`,
    // transition: theme.transitions.create("width"),
    width: "100%",
  },
  close: {
    color: theme.palette.grey[600],
    position: "absolute",
    right: theme.spacing(1),
    display: "flex",
    alignItems: "center",
    "&:hover": {
      cursor: "pointer",
      color: "red",
    },
  },
  icon_button: {
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
  },
  button: {
    color: theme.palette.common.black,
    "&:hover": {
      backgroundColor: "transparent",
      color: theme.palette.grey[600],
    },
  },
}));

export default function TopNav() {
  const [displaySearchBar, setDisplaySearchBar] = useState(false);

  const classes = useStyles(displaySearchBar);
  const handleSearchIconOnClick = (event) => {
    setDisplaySearchBar(true);
  };
  return (
    <Grid
      item
      container
      direction="row"
      alignItems="center"
      justify="space-between"
      className={classes.topnav}
    >
      {!displaySearchBar && (
        <Hidden smDown>
          <Grid item xs={4} className={classes.country}>
            <img src="/images/bd_flag.png" alt="Bangladesh Flag" />
            <Typography variant="overline">Bangladesh</Typography>
            {/*<img src="/images/taka.png" alt="Bangladesh Taka" />*/}
          </Grid>
        </Hidden>
      )}

      {!displaySearchBar && (
        <Hidden mdUp>
          <Menus />
        </Hidden>
      )}

      <Hidden xsDown>
        <Grid item xs={displaySearchBar ? 3 : 4} className={classes.brand}>
          <Link href={"/"} passHref>
            <Typography variant="button">BeautyCareLondon</Typography>
          </Link>
        </Grid>
      </Hidden>
      <Hidden smUp>
        <Grid item xs={displaySearchBar ? 3 : 4} className={classes.brand}>
          <Link href={"/"} passHref>
            <Typography variant="button">BCL</Typography>
          </Link>
        </Grid>
      </Hidden>

      {displaySearchBar && (
        <Grid item xs={6}>
          <div className={classes.search}>
            <div className={classes.search_icon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search ..."
              classes={{
                root: classes.input_root,
                input: classes.input_input,
              }}
              inputProps={{ "aria-level": "search" }}
              fullWidth
              margin="dense"
              onBlur={() => setDisplaySearchBar(false)}
              onChange={() => console.log("To Be Implemented!!!")}
            />
            <div
              className={classes.close}
              onClick={() => setDisplaySearchBar(false)}
            >
              <Hidden smDown>
                <Typography variant="overline">Close</Typography>
              </Hidden>
              <CloseIcon />
            </div>
          </div>
        </Grid>
      )}

      <Grid
        item
        xs={displaySearchBar ? 3 : 4}
        container
        direction="row"
        alignItems="center"
        justify="flex-end"
        className={classes.icon_button}
      >
        {!displaySearchBar && (
          <Grid item>
            <IconButton
              size="small"
              disableRipple
              classes={{ root: classes.button }}
              onClick={handleSearchIconOnClick}
              onTouchStart={handleSearchIconOnClick}
            >
              <SearchOutlinedIcon />
            </IconButton>
          </Grid>
        )}

        <Hidden smDown>
          <Grid item>
            <IconButton
              size="small"
              disableRipple
              classes={{ root: classes.button }}
            >
              <FavoriteBorderOutlinedIcon />
            </IconButton>
          </Grid>
          <Grid item>
            <IconButton
              size="small"
              disableRipple
              classes={{ root: classes.button }}
            >
              <PersonOutlineOutlinedIcon />
            </IconButton>
          </Grid>
        </Hidden>
        <Grid item>
          <IconButton
            size="small"
            disableRipple
            classes={{ root: classes.button }}
          >
            <LocalMallOutlinedIcon />
          </IconButton>
        </Grid>
      </Grid>
    </Grid>
  );
}
