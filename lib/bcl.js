// list of menus

class Menu {
  constructor(label, href) {
    this.label = label;
    this.href = href;
  }
}

const menus = [
  new Menu("Men", "/men"),
  new Menu("Women", "/women"),
  new Menu("Kids", "/kids"),
  new Menu("Deals", "/deals"),
  new Menu("New Arrivals", "/newarrivals"),
];

export { menus };
