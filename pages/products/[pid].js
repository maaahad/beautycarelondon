// react

// next
import { useRouter } from "next/router";

export default function Product() {
  const router = useRouter();
  const { pid } = router.query;
  return <h1>This is description of the product : {pid}</h1>;
}
