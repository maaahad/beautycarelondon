// react

// next
import Head from "next/head";
// components
import Content from "../component/home/content";

export default function Home() {
  return (
    <>
      <Head>
        <title>BeautyCareLondon - Home</title>
      </Head>
      <Content />
    </>
  );
}
