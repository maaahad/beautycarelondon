// references for JSS issue for fast refresh in NextJS
// https://itnext.io/next-js-with-material-ui-7a7f6485f671
// https://github.com/mui-org/material-ui/blob/next/examples/nextjs/pages/_document.js

/* eslint-disable react/jsx-filename-extension */
import React from "react";
import Document, { Html, Main, Head, NextScript } from "next/document";
import { ServerStyleSheets } from "@material-ui/core/styles";

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          />
          <link rel="icon" href="/images/logos/bcl.jpeg" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

// `getInitialProps` belongs to `_document` (instead of `_app`),
// it's compatible with server-side generation (SSG).
MyDocument.getInitialProps = async (ctx) => {
  // Render app and page and get the context of the page with collected side effects.
  const sheets = new ServerStyleSheets();
  const originalRenderPage = ctx.renderPage;

  ctx.renderPage = () =>
    originalRenderPage({
      enhanceApp: (App) => (props) => sheets.collect(<App {...props} />),
    });

  const initialProps = await Document.getInitialProps(ctx);

  return {
    ...initialProps,
    // Styles fragment is rendered after the app and page rendering finish.
    styles: [
      ...React.Children.toArray(initialProps.styles),
      sheets.getStyleElement(),
    ],
  };
};

// import Document, { Html, Head, Main, NextScript } from "next/document";
//
// // refenrence : https://medium.com/wesionary-team/implementing-react-jss-on-next-js-projects-7ceaee985cad
//
// class MyDocument extends Document {
//   render() {
//     return (
//       <Html>
//         <Head>
//           <link rel="icon" href="/images/logos/bcl.jpeg" />
//         </Head>
//         <body>
//           <Main />
//           <NextScript />
//         </body>
//       </Html>
//     );
//   }
// }
//
// export default MyDocument;
